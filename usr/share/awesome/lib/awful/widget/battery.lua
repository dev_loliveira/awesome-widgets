-- awesome v.3.4
-- Location : /usr/share/awesome/lib/awful/widget/battery.lua
-- You will need to include this module at: /usr/share/awesome/lib/awful/init.lua
-- Instantiate this widget at : /etc/xdg/rc.lua (next to mytextclock)


function readBatFile(adapter, ...)
  local basepath = "/sys/class/power_supply/"..adapter.."/"
  for i, name in pairs({...}) do
    file = io.open(basepath..name, "r")
    if file then
      local str = file:read()
      file:close()
      return str
    end
  end
end


local function batteryInfo(adapter)
  local fh = io.open("/sys/class/power_supply/"..adapter.."/present", "r")
  if fh == nil then
    battery = "A/C"
    icon = ""
    percent = ""
  else
    local cur = readBatFile(adapter, "charge_now", "energy_now")
    local cap = readBatFile(adapter, "charge_full", "energy_full")
    local sta = readBatFile(adapter, "status")
    battery = math.floor(cur * 100 / cap)

    if sta:match("Charging") then
      icon = "⚡"
      percent = "%"
    elseif sta:match("Discharging") then
      icon = ""
      percent = "%"
      if tonumber(battery) < 15 then
      end
    else
      -- If we are neither charging nor discharging, assume that we are on A/C
      battery = "A/C"
      icon = ""
      percent = ""
    end

    -- fix 'too many open files' bug on awesome 4.0
    fh:close()
  end
  return " "..icon..battery..percent.." "
end




local setmetatable = setmetatable
local os = os
local capi = { widget = widget,
               timer = timer }

--- Text clock widget.
module("awful.widget.battery")

--- Create a textclock widget. It draws the time it is in a textbox.
-- @param args Standard arguments for textbox widget.
-- @param format The time format. Default is " %a %b %d, %H:%M ".
-- @param timeout How often update the time. Default is 60.
-- @return A textbox widget.
function new(args, format, timeout)
    local BAT = "BAT1"
    local args = args or {}
    local format = format or " %a %b %d, %H:%M "
    local timeout = timeout or 5
    args.type = "textbox"
    local w = capi.widget(args)
    local timer = capi.timer { timeout = timeout }
    w.text = batteryInfo(BAT)
    timer:add_signal("timeout", function() w.text = batteryInfo(BAT) end)
    timer:start()
    return w
end

setmetatable(_M, { __call = function(_, ...) return new(...) end })

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
